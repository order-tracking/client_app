FROM node:7
ARG REACT_APP_API_URL=https://api-staging.otrack.info
COPY . /client
ENV CI=true
# development so we install devDependecies and can run react-scripts build
# in the build stage of Create React App, the NODE_ENV is set to production
ENV NODE_ENV=development
ENV REACT_APP_API_URL=${REACT_APP_API_URL}
WORKDIR /client
EXPOSE 9000
RUN curl -o- -L https://yarnpkg.com/install.sh | bash
RUN yarn install
RUN yarn build
WORKDIR /client/server
RUN yarn install
CMD ["yarn","start"]
