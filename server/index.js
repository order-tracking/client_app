const express = require('express')
const path = require('path')
const os = require('os')
const app = express()

const buildFolder = '../build'

app.use(express.static(path.join(__dirname, buildFolder)))

// For test purposes only
app.get('/hostname', (req, res) => {
  res.send(
    'hostname: ' + os.hostname() + ' uptime: ' + Math.floor(os.uptime()) + 's'
  )
})

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, buildFolder, 'index.html'))
})

app.listen(9000)
