import React, { Component } from 'react'
import './App.css'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Navbar from './components/Navbar'
import Home from './components/Home'
import MainContent from './components/order/MainContent'
import Feedback from './components/feedback/Feedback'
import NotFound from './components/NotFound'
import {Provider} from "react-redux"
import configureStore from "./redux/store"

const store = configureStore()

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <Navbar />
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/:id" component={MainContent} />
              <Route exact path="/:id/feedback" component={Feedback} />
              <Route component={NotFound} />
            </Switch>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
