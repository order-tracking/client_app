import React, { Component } from 'react'

import { Image } from 'semantic-ui-react'

import logo from '../assets/img/otrack_vertical.svg'

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <div>
        <Image src={logo} alt="oTrack" size="big" centered />
      </div>
    )
  }
}

export default Home
