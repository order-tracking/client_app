import React, { PureComponent } from 'react'
import { Menu } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'

import icon from '../assets/img/otrack_vertical_white.svg'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'

class Navbar extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { menu } = this.props
    return (
      <Menu className="no-border-radius" inverted>
        <Menu.Item className="item-no-border">
          <img src={icon} alt="Logo" />
        </Menu.Item>
        <Menu.Menu position="right">
          {menu.menuItems.map((item, idx) => {
            return (
              <Menu.Item
                key={idx}
                className="item-no-border"
                as={NavLink}
                to={item.link}
              >
                {item.name}
              </Menu.Item>
            )
          })}
        </Menu.Menu>
      </Menu>
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    menu: state.menu
  }
}

export default connect(mapStateToProps)(withRouter(Navbar))
