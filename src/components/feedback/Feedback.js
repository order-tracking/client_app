import React, { Component } from 'react'
import {
  Button,
  Divider,
  Form,
  Header,
  Rating,
  Segment,
  TextArea
} from 'semantic-ui-react'
import FeedbackRepository from '../../data/FeedbackRepository'
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { setMenu } from '../../redux/reducers/menuReducer'

class Feedback extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      feedback: {
        orderHashedId: this.props.match.params.id,
        rating: 3,
        note: ''
      }
    }

    this.handleChangeRating = (event, { rating }) =>
      this.setState({
        feedback: {
          ...this.state.feedback,
          rating: rating
        }
      })
    this.handleChangeTextArea = (event, { value }) =>
      this.setState({
        feedback: {
          ...this.state.feedback,
          note: value
        }
      })

    this.submitFeedback = this.submitFeedback.bind(this)
  }

  componentDidMount() {
    this.props.actions.setMenu([
      { name: 'Back', link: '/' + this.props.match.params.id }
    ])

    FeedbackRepository.getOrderFeedback(this.state.feedback.orderHashedId)
      .then(feedback => {
        console.log(feedback)
        this.setState({ feedback, isLoading: false })
      })
      .catch(e => {
        this.setState({ isLoading: false })
      })
  }

  submitFeedback() {
    if (this.state.feedback.id) {
      //PUT
      FeedbackRepository.editFeedback(this.state.feedback)
        .then(feedback => alert('Edited Successfully'))
        .catch(error => alert('Impossible to edit. Please try again'))
    } else {
      //CREATE
      FeedbackRepository.createFeedback(this.state.feedback)
        .then(feedback => alert('Created Successfully'))
        .catch(error => alert('Impossible to create. Please try again'))
    }
  }

  render() {
    const { feedback, isLoading } = this.state
    return (
      <Segment basic textAlign="center" loading={isLoading}>
        <Header as="h1">Share with us your opinion</Header>
        <Divider />
        <Segment basic clearing padded textAlign="center">
          <Form>
            <Header textAlign="left" floated="left">
              Rate our service:{' '}
            </Header>
            <Rating
              icon="star"
              size="massive"
              onRate={this.handleChangeRating}
              rating={feedback.rating}
              maxRating={5}
            />
            <Header textAlign="left">Leave us a comment: </Header>
            <TextArea
              placeholder="Tell us more"
              value={feedback.note}
              onChange={this.handleChangeTextArea}
              autoHeight
            />
            <Divider hidden />
            <Button
              attached="bottom"
              type="button"
              content="Submit"
              onClick={this.submitFeedback}
            />
          </Form>
        </Segment>
      </Segment>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {}
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ setMenu }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withRouter(Feedback)
)
