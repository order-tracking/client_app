import React, { Component } from 'react'
import { Container, Segment } from 'semantic-ui-react'
import OrderRepository from '../../data/OrderRepository'
import { withRouter } from 'react-router'
import OrderSteps from './OrderSteps'
import OrderDetails from './OrderDetails'
import OrderDeliveryInfo from './OrderDeliveryInfo'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { setMenu } from '../../redux/reducers/menuReducer'

class MainContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      order: {
        delivered: false,
        readyToDistribute: false,
        items: []
      }
    }

    this.getData = this.getData.bind(this)
    this.updateView = this.updateView.bind(this)
  }

  getData() {
    OrderRepository.getOrderByHashedId(this.props.match.params.id)
      .then(order => {
        console.log(order)
        this.updateView(order)
      })
      .catch(error => {
        this.props.history.push('/')
      })
  }

  updateView(order) {
    if (order.deliveredAt) {
      this.props.actions.setMenu([
        { name: 'Feedback', link: '/' + order.hashId + '/feedback' }
      ])
    }

    this.setState({
      isLoading: false,
      order
    })
  }

  componentDidMount() {
    this.getData()
    this.intervalId = setInterval(this.getData, 60000) // 1 min
  }

  componentWillUnmount() {
    clearInterval(this.intervalId)
  }

  render() {
    const { order, isLoading } = this.state
    return (
      <Container fluid as={Segment} basic loading={isLoading}>
        {!order.deliveredAt && <OrderDeliveryInfo order={order} />}
        <OrderSteps order={order} />
        <OrderDetails order={order} />
      </Container>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {}
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ setMenu }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withRouter(MainContent)
)
