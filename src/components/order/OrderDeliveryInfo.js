import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Segment, Statistic } from 'semantic-ui-react'
import Moment from 'react-moment'
import moment from 'moment'
import * as DateHelpers from '../../helpers/DateHelpers'

class OrderDeliveryInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentTime: moment.utc(props.order.estimatedDate).local()
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.order.estimatedDate) {
      this.setState({
        currentTime: moment.utc(nextProps.order.estimatedDate).local()
      })
    }
  }

  render() {
    const diff = DateHelpers.differenceCurrentDate(this.state.currentTime)
    return (
      <Segment vertical textAlign="center">
        {diff > 0 &&
          <Statistic>
            <Statistic.Value>
              <Moment date={this.state.currentTime} format="HH:mm" />
            </Statistic.Value>
            <Statistic.Label>Estimated Delivery Time</Statistic.Label>
          </Statistic>}
        {diff <= 0 &&
          <Statistic>
            <Statistic.Value>
              {diff * -1}
            </Statistic.Value>
            <Statistic.Label>Minutes Late</Statistic.Label>
          </Statistic>}
      </Segment>
    )
  }
}

OrderDeliveryInfo.propTypes = {
  order: PropTypes.object.isRequired
}

export default OrderDeliveryInfo
