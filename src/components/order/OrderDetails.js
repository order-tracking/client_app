import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Header, Icon, List, Segment } from 'semantic-ui-react'

class OrderDetails extends Component {
  render() {
    const { order } = this.props
    return (
      <Segment padded>
        <Header>Order Details:</Header>
        <List>
          {order.items.map(item =>
            <List.Item key={item.id}>
              <Icon name="triangle right" />
              <List.Content>
                <List.Header>
                  {item.product.name + ' '}
                  {item.note ? ' (' + item.note + ')' : ''}
                </List.Header>
                <List.Description>
                  Quantity: {item.quantity}
                </List.Description>
              </List.Content>
            </List.Item>
          )}
        </List>
      </Segment>
    )
  }
}

OrderDetails.propTypes = {
  order: PropTypes.object.isRequired
}

export default OrderDetails
