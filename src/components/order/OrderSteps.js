import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Step } from 'semantic-ui-react'
import StepItem from '../../models/Step'

class OrderSteps extends Component {
  static ORDER_INPROGRESS = 'Your order is being processed'
  static ORDER_READY = 'Your order is ready'

  static WAITING_DISTRIBUTOR = 'Waiting for distributor'
  static DISTRIBUTOR_ONWAY = 'Our distributor is on their way!'
  static DISTRIBUTOR_DONE = 'Our distributor has already delivered'

  static FOOD_DELIVERED = 'Enjoy your food!'

  constructor(props) {
    super(props)
    this.state = {
      steps: [
        new StepItem(false, false, 'Processing', this.ORDER_INPROGRESS),
        new StepItem(false, false, 'Transport', this.WAITING_DISTRIBUTOR),
        new StepItem(false, false, 'Delivered')
      ]
    }

    this.updateView = this.updateView.bind(this)
  }

  updateView() {
    const { order } = this.props
    const { steps } = this.state

    this.setState({
      steps: [
        {
          ...steps[0],
          completed: order.readyToDistribute,
          active: !order.readyToDistribute,
          description: order.readyToDistribute
            ? OrderSteps.ORDER_READY
            : OrderSteps.ORDER_INPROGRESS
        },
        {
          ...steps[1],
          completed: order.deliveredAt !== undefined,
          active: order.onGoing,
          description:
            order.deliveredAt !== undefined
              ? OrderSteps.DISTRIBUTOR_DONE
              : order.onGoing
                ? OrderSteps.DISTRIBUTOR_ONWAY
                : OrderSteps.WAITING_DISTRIBUTOR
        },
        {
          ...steps[2],
          completed: order.deliveredAt !== undefined,
          description:
            order.deliveredAt !== undefined
              ? OrderSteps.FOOD_DELIVERED
              : undefined
        }
      ]
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.order !== this.props.order) this.updateView()
  }

  componentDidMount() {
    this.updateView()
  }

  render() {
    const { steps } = this.state
    return (
      <Step.Group fluid ordered stackable="tablet">
        {steps.map((step, idx) =>
          <Step
            key={idx}
            completed={step.completed}
            active={step.active}
            title={step.title}
            description={step.description}
          />
        )}
      </Step.Group>
    )
  }
}

OrderSteps.propTypes = {
  order: PropTypes.object.isRequired
}

export default OrderSteps
