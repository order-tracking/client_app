import axios from 'axios'

class FeedbackRepository {
  static getOrderFeedback(orderHashedId) {
    return axios
      .get('/orders/client/' + orderHashedId + '/feedback')
      .then(resp => resp.data)
  }

  static createFeedback(feedback) {
    return axios.post('/feedbacks', feedback).then(resp => resp.data)
  }

  static editFeedback(feedback) {
    return axios
      .put('/feedbacks/' + feedback.id, feedback)
      .then(resp => resp.data)
  }
}

export default FeedbackRepository
