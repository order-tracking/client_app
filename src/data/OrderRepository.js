import axios from 'axios'

class OrderRepository {
  static getOrderById(id) {
    return axios.get('/orders/' + id).then(resp => resp.data)
  }

  static getOrderByHashedId(hashId) {
    return axios.get('/orders/client/' + hashId).then(resp => resp.data)
  }
}

export default OrderRepository
