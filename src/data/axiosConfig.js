import axios from 'axios'

/**
 * Axios default config for every request
 * Loaded in index.js
 *
 * More info:
 * https://github.com/mzabriskie/axios
 */
axios.defaults.baseURL = process.env.REACT_APP_API_URL
axios.defaults.timeout = 5000
axios.defaults.responseType = 'json'
//axios.defaults.headers.post['Content-Type'] = 'application/json'
