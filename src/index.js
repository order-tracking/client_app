import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import './index.css'

import 'semantic-ui-css/semantic.min.css'
import './data/axiosConfig'

ReactDOM.render(<App />, document.getElementById('root'))
registerServiceWorker()

// Check env vars
console.log('API_URL', process.env.REACT_APP_API_URL)
