class Step {
  constructor(completed, active, title, description) {
    this.completed = completed
    this.active = active
    this.title = title
    this.description = description
  }
}

export default Step
