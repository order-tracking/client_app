/*
 |--------------------------------------------------------------------------
 | Types
 |--------------------------------------------------------------------------
 */
import * as helpers from '../helpers'

export const SET_MENU = 'otrack/client/SET_MENU'
export const RESET_MENU = 'otrack/client/RESET_MENU'

/*
 |--------------------------------------------------------------------------
 | Reducer
 |--------------------------------------------------------------------------
 */
const initialState = {
  menuItems: []
}

export default helpers.createReducer(initialState, {
  [SET_MENU]: (state, action) => {
    return { ...state, menuItems: action.menuItems }
  },
  [RESET_MENU]: (state, action) => {
    return initialState
  }
})

/*
|--------------------------------------------------------------------------
| Action Creators
|--------------------------------------------------------------------------
*/
export function setMenu(menuItems) {
  return { type: SET_MENU, menuItems }
}

// export function resetMenu() {
//   return {type: RESET_MENU}
// }
