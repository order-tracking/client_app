import { combineReducers } from 'redux'
import menuReducer from './reducers/menuReducer'

// Where we can add more reducers
const rootReducer = combineReducers({
  menu: menuReducer
})

export default rootReducer
